const fs = require('fs');
const path = require('path');

// Create folder
// fs.mkdir(path.join(__dirname, 'test'), {}, (err) => {
//     if (err) throw err;
//     console.log(`Folder created...`);
// });
//
// // Create and write to the file
// fs.writeFile(path.join(__dirname, 'test', 'hello.txt'), 'Hello World!', err => {
//     if (err) throw err;
//     console.log('File created...');
//
//     // Append file
//     fs.appendFile(path.join(__dirname, 'test', 'hello.txt'), 'I love NodeJS', err => {
//         if (err) throw err;
//         console.log('File updated...')
//     })
// });
//


// // Read file
// fs.readFile(path.join(__dirname, 'test', 'hello.txt'), 'utf8', ((err, data) => {
//     if (err) throw err;
//     console.log(data);
// }));

// Rename file
fs.rename(path.join(__dirname, 'test', 'hello.txt'), path.join(__dirname, 'test', 'hellowordl.txt'), err => {
    if (err) throw err;
    console.log('File renamed...');
});